package se.experis.diaryApp.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import com.zaxxer.hikari.*;

@Configuration
public class DatabaseConfig {

    //Config file for DB-connection
    @Value("${spring.datasource.url}")
    private String dbUrl;

    //Using hikari for handling db, (Speed)
    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(dbUrl);
        return new HikariDataSource(config);
    }
}


