package se.experis.diaryApp.Controllers;

import org.aspectj.lang.annotation.DeclareError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.diaryApp.Repositories.DiaryEntryRepository;
import se.experis.diaryApp.models.CommonResponse;
import se.experis.diaryApp.models.DiaryEntry;
import se.experis.diaryApp.utils.Command;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class DiaryRestController {

    @Autowired
    private DiaryEntryRepository diaryEntryRepository;

    //The only post (add) as JSON.
    @PostMapping("/diaryEntry")
    public ResponseEntity<CommonResponse> addDiaryEntry(HttpServletRequest request, @RequestBody DiaryEntry entry) {
        Command cmd = new Command(request);
        CommonResponse cr = new CommonResponse();

        //Automatically attaching a date of following format.
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        //The date is single-handedly connected to entry values.
        entry.date = dtf.format(now);

        //Saving the full row to entry.
        entry = diaryEntryRepository.save(entry);

        cr.data = entry;
        cr.message = "New diaryentry with id: " + entry.id;

        HttpStatus resp = HttpStatus.CREATED;
        cmd.setResult(resp);
        return new ResponseEntity<>(cr, resp);
    }
        //Similair, but fetching values fron ONE (id)
        //Never eused in frontend, just a bunch of JSON
    @GetMapping("/diary/{id}")
    public ResponseEntity<CommonResponse> getDiaryById(HttpServletRequest request, @PathVariable Integer id){
        Command cmd = new Command(request);

        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(diaryEntryRepository.existsById(id)) {
            cr.data = diaryEntryRepository.findById(id);
            cr.message = "Diary with id: " + id + " exists.";
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Diary not found.";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }
    //Never eused in frontend, just a bunch of JSON
    @GetMapping("/diary")
    public ResponseEntity <CommonResponse> getAllDiary(HttpServletRequest request){

        Command cmd = new Command(request);
        CommonResponse cr = new CommonResponse();

        //cr.data = diaryEntryRepository.findAll();

        cr.data = diaryEntryRepository.findAll(Sort.by(Sort.Direction.DESC, "date"));

        cr.message = "All diary entries!";

        HttpStatus resp = HttpStatus.OK;

        return new ResponseEntity<>(cr, resp);
    }
        //Importing a id from url, deleting the corresponding entry from db.
    @DeleteMapping ("/diary/delete/{id}")
    public ResponseEntity <CommonResponse> deleteDiary(HttpServletRequest request, @PathVariable Integer id) {

        Command cmd = new Command(request);
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(diaryEntryRepository.existsById(id)){
            diaryEntryRepository.deleteById(id);
            cr.message = "Deleted the diary id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Diary not found!";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(cr, resp);
    }
        //USed for editing diaryEntries. If statement for all the values. IF id exists
    @PatchMapping ("/diary/edit/{id}")
    public ResponseEntity <CommonResponse> editDiary(HttpServletRequest request, @RequestBody DiaryEntry newEntry, @PathVariable Integer id) {
        Command cmd = new Command(request);
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(diaryEntryRepository.existsById(id)) {
            Optional<DiaryEntry> diaryRepo = diaryEntryRepository.findById(id);

            DiaryEntry diaryEntry = diaryRepo.get();
            System.out.println(diaryRepo.get());

            if (newEntry.diaryTitle != null) {
                diaryEntry.diaryTitle = newEntry.diaryTitle;
            }
            if (newEntry.diaryText != null) {
                diaryEntry.diaryText = newEntry.diaryText;
            }
            if (newEntry.image != null) {
                diaryEntry.image = newEntry.image;
            }
            if (newEntry.date != null) {
                diaryEntry.date = newEntry.date;
            }

            diaryEntryRepository.save(diaryEntry);
            cr.data = newEntry;
            cr.message = "The id nr: " + id + " was updated";
            resp = HttpStatus.OK;
        }
            else {
             cr.message = "NOT FOUND";
                resp = HttpStatus.NOT_FOUND;
            }

        return new ResponseEntity<>(cr, resp);
    }

}
