package se.experis.diaryApp.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import se.experis.diaryApp.Repositories.DiaryEntryRepository;
import se.experis.diaryApp.models.DiaryEntry;

import java.util.List;
import java.util.Optional;

@Controller
public class DiaryController {

    //By marking with autowire, spring will take care of instanziation.
    @Autowired
    DiaryEntryRepository diaryEntryRepository;
    @GetMapping("/")
    public String diaryController(Model model){
        //Creating a list, finding sorted values from repo to put into
        //Instead of findingAll on id, we find all on date, while also sorting.
        List<DiaryEntry> entries = diaryEntryRepository.findAll(Sort.by(Sort.Direction.DESC, "date"));
        model.addAttribute("entries", entries);
        //Returning (sending) the page with the db-info included
        return "index.html";
    }

    //Getting into action of the URL-id, using built-in method findById to put correct data into object called entry.
    @GetMapping("/edit/{id}")
    public String editController(@PathVariable("id") String id, Model model) {
        DiaryEntry entry= diaryEntryRepository.findById(Integer.parseInt(id)).get();
        model.addAttribute("entry", entry);
        //Below returns ONE entry to frontend.
        return "editEntry.html";
    }

    @GetMapping("/addEntry")
    public String addControler(){
        return "addEntry.html";
    }
}




