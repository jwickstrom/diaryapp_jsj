package se.experis.diaryApp.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.diaryApp.models.DiaryEntry;

import java.util.List;

//Interface for repository
public interface DiaryEntryRepository extends JpaRepository<DiaryEntry, Integer> {

    //DiaryEntry getById(String id);
    //List<DiaryEntry> getAllOrderByDateAsc(String date);
}
