package se.experis.diaryApp.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

//Making the class an entity for the framework.
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id"
)
//Model of all variables.
public class DiaryEntry {
    //Genereated an id for diaryentry which auto-increments
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    //Everey attribute is made into a column in the db.
    @Column
    public String diaryTitle;
    @Column
    public String diaryText;
    @Column
    public String date;
    @Column
    public String image;
}
