package se.experis.diaryApp.models;

public class CommonResponse {
    //Class for storing oject and a corresponding message string
    public Object data;
    public String message;
}
